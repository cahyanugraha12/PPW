from django.urls import re_path
import ppw.views as views

urlpatterns = [
    re_path(r'^personal/$', views.personal, name='personal'),
    re_path(r'^cv/$', views.cv, name='cv'),
    re_path(r'^interest/$', views.interest, name='interest'),
    re_path(r'^registration/$', views.registration, name='registration'),
    re_path(r'^personal-schedule/$', views.personal_schedule, name='personal-schedule'),
    re_path(r'^personal-schedule/(?P<pk>[0-9]+)/$', views.personal_schedule_detail, name='personal-schedule-detail'),
]