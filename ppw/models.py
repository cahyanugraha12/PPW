from django.db import models


class PersonalSchedule(models.Model):
    date = models.DateField() 
    time = models.TimeField()
    activity_name = models.CharField(max_length=255)
    place = models.CharField(max_length=255)
    category = models.CharField(max_length=255, default="Random")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
