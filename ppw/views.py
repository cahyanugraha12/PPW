from django.shortcuts import render, redirect
from ppw.models import PersonalSchedule
from ppw.forms import PersonalScheduleForm

def personal(request):
    title = 'Personal - Pande Ketut Cahya Nugraha'
    response = {'title': title}
    return render(request, 'ppw/personal.html', response)


def cv(request):
    title = 'CV - Pande Ketut Cahya Nugraha'
    response = {'title': title}
    return render(request, 'ppw/cv.html', response)


def interest(request):
    title = 'Interest - Pande Ketut Cahya Nugraha'
    response = {'title': title}
    return render(request, 'ppw/interest.html', response)


def registration(request):
    title = 'Registration - Pande Ketut Cahya Nugraha'
    response = {'title': title}
    return render(request, 'ppw/registration.html', response)


def personal_schedule(request):
    ps_list = PersonalSchedule.objects.all()
    ps_form = PersonalScheduleForm()
    if request.method == "POST":
        sub_method = request.POST.get('_method', '').upper()
        if sub_method == "DELETE":
            ps_list.delete()
            return redirect('personal-schedule')
        else:
            ps_form = PersonalScheduleForm(request.POST)
            if ps_form.is_valid():
                ps_form.save()
                return redirect('personal-schedule')
    response = {'ps_list': ps_list, 'ps_form': ps_form}
    return render(request, 'ppw/personal_schedule.html', response)

def personal_schedule_detail(request, pk):
    if request.method == "POST":
        sub_method = request.POST.get('_method', '').upper()
        if sub_method == "DELETE":
            PersonalSchedule.objects.filter(id=pk).delete()
            return redirect('personal-schedule')
        