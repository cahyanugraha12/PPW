from django import forms
from ppw.models import PersonalSchedule


class PersonalScheduleForm(forms.ModelForm):
    class Meta:
        model = PersonalSchedule
        fields = ['activity_name', 'date', 'time', 'place', 'category']
        widgets = {
            'activity_name': forms.TextInput(attrs={'type': 'text', 'class': 'form-control'}),
            'date': forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}), 
            'time': forms.TimeInput(attrs={'type': 'time', 'class': 'form-control'}), 
            'place': forms.TextInput(attrs={'type': 'text', 'class': 'form-control'}),
            'category': forms.TextInput(attrs={'type': 'text', 'class': 'form-control'}),
        }