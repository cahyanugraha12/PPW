from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Pande Ketut Cahya Nugraha'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 4, 12)


def homepage(request):
    title = 'Homepage - Pande Ketut Cahya Nugraha'
    response = {'title': title}
    return render(request, 'homepage.html', response)


def index(request):
    npm = 1706028663
    current_institution = 'Universitas Indonesia'
    hobbies = 'Playing Video Games, Reading Novel, and Watching We Bare Bears'
    short_description = 'Ik ben een jongen (That means "I am a boy") who like simple things in life'
    photo_url = 'MyPhoto.jpg'

    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm,
                'current_institution': current_institution, 'hobbies': hobbies,
                'short_description': short_description, 'photo_url': photo_url}
    return render(request, 'profile.html', response)


def frontfriend_profile(request):
    name = 'Tembok Lab'
    frontfriend_birth_date = date(1993, 12, 31)
    npm = 0000000000
    current_institution = 'Universitas Indonesia'
    hobbies = 'Melihat mahasiswa-mahasiswa yang stres mengerjakan lab'
    short_description = 'Saya berwarna putih dan keras'
    photo_url = 'Tembok.jpg'

    response = {'name': name, 'age': calculate_age(frontfriend_birth_date.year), 'npm': npm,
                'current_institution': current_institution, 'hobbies': hobbies,
                'short_description': short_description, 'photo_url': photo_url}
    return render(request, 'profile.html', response)


def backfriend_profile(request):
    name = 'Naufal Pratama Tanansyah'
    backfriend_birth_date = date(1999, 10, 21)
    npm = 1706979410
    current_institution = 'Universitas Indonesia'
    hobbies = 'Drawing and Reading'
    short_description = 'Saya adalah orang yang pendiam dan senang memperhatikan orang lain di sekitar saya.\
                         Saya juga tertarik dalam hal-hal di bidang seni dan komputer'
    photo_url = 'PhotoTan.png'

    response = {'name': name, 'age': calculate_age(backfriend_birth_date.year), 'npm': npm,
                'current_institution': current_institution, 'hobbies': hobbies,
                'short_description': short_description, 'photo_url': photo_url}
    return render(request, 'profile.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
