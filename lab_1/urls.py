from django.urls import re_path
from .views import index, frontfriend_profile, backfriend_profile
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index_self_profile'),
    re_path(r'^profile/$', index, name='self_profile'),
    re_path(r'^frontfriend-profile/$', frontfriend_profile, name='frontfriend_profile'),
    re_path(r'^backfriend-profile/$', backfriend_profile, name='backfriend_profile'),
]
